const sqlite3 = require('sqlite3').verbose();
const conf = require('../config.json');
const Discord = require('discord.js');

class Trivia {

	constructor (message) {
      this.guild     = message.guild;
      this.channel   = message.channel;
      this.message   = message;
      this.dbpath    = "./db/data.sqlite";
	  this.timeoutTracker  = null; // Do not edit. Carries timer references.
	  this.activeQuestion = null; // Do not edit. Carries question reference
	  this.questionCount = 0; // Do not edit, tracks questions
	  this.debugmode = conf.debugmode; // Boolean. Prints debug info to console when enabled.
      this.secondsToAnswerQuestions = conf.secondsToAnswerQuestions; // How long do you have to guess the answer before it times out? Default 60
      this.secondsBeforeNextQuestion = conf.secondsBeforeNextQuestion; // Question times out or is answered, how long before the next one? Default 8
      this.questionValue = conf.questionValue;  // Maximum number of points earned for answering a question, decreases over time. Default 100
	  
	}

	/**
	* Parse a message and decide what to do based on current state
	*/
	handleMessage() {
	    
	    let message = this.message;
		let msg = message.content;
		let prefix = conf.prefix;
		
		// Were we trying to send a command?
		if(msg.startsWith(prefix)){
			if(msg == `${prefix}t` || msg == `${prefix}trivia`){ // Start/stop
				this.toggleGame();
			} else if(msg == `${prefix}leaders` || msg == `${prefix}leaderboard`){ // Leaderboard
				this.leaderboard();
			} else if(msg == `${prefix}help` || msg == `${prefix}commands`){ // Help
				this.commands();
			} else if(msg.includes(`${prefix}stats`)){ // User stats
				this.userStats();
			} else if(msg == `${prefix}ranks`){ // See ranks
				this.listRanks();
			} else if(msg == `${prefix}triviastats`){ // See trivia stats (count questions, users, etc)
				this.triviaStats();
			} else if(msg == `${prefix}skip`){ // Skip this question
				this.skipQuestion();
			} else if(msg == `${prefix}hint`){ // Get a hint
				this.sendHint();
			} else if(msg.includes(`${prefix}flag`)){ // Flag a question for review
				this.flagQuestion();
			}
		} else {
	    	this.checkAnswer(msg);
	    }
	};

	/**
	 * Check an answer against an active game to see if it satisfies the current question
	 * @param {*} answer 
	 */
    checkAnswer(answer){

    	let trivia = this;

    	// Format input 
		answer = answer.trim().toLowerCase();
		this.debug("Checking answer: " + answer);

    	// Ignore empty answers
    	if(answer == ""){
    		return;
    	}
    	
    	// If game is active, check answer
		this.gameIsActive().then( gameIsActive => {
		    if (gameIsActive) {
				
		    	// Connect to DB
		    	let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
					if (err) {
						reject(err);
					} else {

						// See if my game's answer is the answer specified 
						let sql = "SELECT asked_at, question_id FROM games WHERE guild = ? AND channel = ? AND answer = ?";
						db.get(sql, [trivia.guild.id, trivia.channel.id, answer], (err, row) => {
							if (err) {
								reject(err.message);
							} else {

								// If we got a row back, answer is correct
								if(row == null){ // Answer incorrect, ignore
								} else { // Answer correct!

									// Did we answer in time?
									let must_answer_by = row.asked_at + trivia.secondsToAnswerQuestions;
									let answered_at = Date.now() / 1000;
									if(answered_at < must_answer_by){
										trivia.correctAnswer(answer, row.asked_at, row.question_id, answered_at);
									}
								}
							}
						});	
					}
				}); 
				db.close();
		    }
		}).catch( err => {
		    this.error(err);
		});

    };

	/**
	 * Toggle the status of the current game
	 */
	toggleGame() {
		this.gameIsActive().then( gameIsActive => {
			if (gameIsActive) {
				this.stop();
			} else {
				this.start();
			}
		}).catch( err => {
			this.error(err);
		});	
	}

    /**
	 * Start a trivia game
	 */
    start(){
    	// Store server/channel and send a question
    	this.debug("Starting Trivia");
		this.setActiveState(true);
		  
		// TODO Promisify this hack 
      	let trivia = this;
		let k = setTimeout(function(){trivia.sendQuestion();}, 2000);
		  
		this.message.channel.send("Trivia starting");
    };

    /**
	 * Stop the trivia game
	 */
    stop(){
    	this.debug("Stopping Trivia");
		this.setActiveState(false);
		this.message.channel.send("Trivia stopped");
    };

    /**
	 * Send a question to the client
	 */
    sendQuestion() {

    	let trivia = this;

    	// If game is active
    	this.gameIsActive().then( gameIsActive => {

		    if (gameIsActive) {

		    	// Load a question
		    	this.loadQuestion().then( question => {

		    		// Connect to DB
			    	let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
						if (err) {
							reject(err);
						} else {

							trivia.debug("Sending new question");

							// Save the question to the game
							let asked_at = Date.now() / 1000;
							let sql = "UPDATE games SET question_id = ?, question = ?, answer = ?, asked_at = ? WHERE guild = ? AND channel = ?";
							db.run(sql, [question.id, question.question, question.answer.trim().toLowerCase(), asked_at, trivia.guild.id, trivia.channel.id]);

							// Send it to the channel
							this.maskAnswer(question.answer).then( masked => {
								
								let formatted_question = `(${question.cat} [${question.difficulty.toLowerCase()}]) ${question.question} (Hint: \`${masked}\`) | ${trivia.secondsToAnswerQuestions}s remaining`;
								trivia.message.channel.send(formatted_question);

								// Make this the new active question an increment question count
								trivia.activeQuestion = question.id; // TODO: This needs to be globalized, not an instance variable
								trivia.questionCount += 1; // TODO: This needs to be globalized, not an instance variable

								// Give the players X seconds to answer the question
								let timeout_timeout = setTimeout(function(){trivia.timeout(question.id)}, trivia.secondsToAnswerQuestions * 1000);
								trivia.timeoutTracker = timeout_timeout;

								// Send hints at intervals until the question is solved
								let seconds_to_first_hint  = Math.round(trivia.secondsToAnswerQuestions * .33);
								let seconds_to_second_hint = Math.round(trivia.secondsToAnswerQuestions * .66);
								let hint_timeout1 		   = setTimeout(function(){trivia.sendHint(question.id)}, seconds_to_first_hint * 1000);
								let hint_timeout2 		   = setTimeout(function(){trivia.sendHint(question.id)}, seconds_to_second_hint * 1000);
								
							});
							

						}
					}); 
					db.close();
		    	});

				
		    } else {
		    	trivia.debug("Cannot send question, game is not active.");
		    }
		}).catch( err => {
		    this.error(err);
		});
    }

    /**
	 * Load a random question from our question vault
	 */
    loadQuestion() {
    	let trivia = this;
    	trivia.debug("Loading new question");
    	return new Promise((resolve, reject) => {

		    // Connect to DB
	    	let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
				if (err) {
					reject(err);
				} else {

					// Avoid previous question
					let previous_question = 0;
					if(trivia.activeQuestion != null){
						previous_question = trivia.activeQuestion;
					}	
					
					// Load a random question from the DB				
					let sql = "SELECT questions.*, cat.name AS cat FROM questions JOIN question_categories AS cat ON cat.id = questions.category WHERE review != 1 AND questions.id != ? ORDER BY RANDOM() LIMIT 1;";
					db.get(sql, [previous_question], (err, row) => {
						if (err) {
							reject(err.message);
						} else {
							if(row == null){
								trivia.error("Failed to load a question");
							} else {
								resolve(row);
							}
							
						}
					});	
				}
			}); 
		});	
	}

	/**
	 * Unload the game's currently active question
	 */
	unloadQuestion() {
		let trivia = this;
    	trivia.debug("Unloading question");
    	return new Promise((resolve, reject) => {

		    // Connect to DB
	    	let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
				if (err) {
					reject(err);
				} else {

					let sql = "UPDATE games SET question_id = 0, question = '', answer = '', asked_at = 0 WHERE guild = ? AND channel = ?";
					db.get(sql, [trivia.guild.id, trivia.channel.id], (err, row) => {
						if (err) {
							reject(err.message);
						} else {
							resolve(row);
						}
					});	
				}
			}); 
		});	
	}

	/**
	 * Skip the current question
	 */
	skipQuestion() {

		let trivia = this;

		// Stop the question timeout
		clearTimeout(this.timeoutTracker);

    	// Connect to DB
    	let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				trivia.error(err);
			} else {

				// Is game/question active?
				let sql = "SELECT question_id FROM games WHERE guild = ? AND channel = ? AND active = 1 AND question_id != 0";
				db.get(sql, [trivia.guild.id, trivia.channel.id], (err, row) => {
					if(row != null){
						
						// Set game as no question
						trivia.unloadQuestion().then( unloaded => {
							
							// Start a new question in x seconds
							let delay = trivia.secondsBeforeNextQuestion * 1000;
							trivia.message.reply(`Skipping question. Next question in ${trivia.secondsBeforeNextQuestion}s`);
							let k = setTimeout(function(){trivia.sendQuestion()}, delay);
						});
					}
				});
			}
		});
	}

	/**
	 * Flag the current question for review and skip it
	 */
	flagQuestion() {
		let trivia = this;

		// Connect to DB
    	let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				trivia.error(err);
			} else {

				// Is game/question active?
				let sql = "SELECT question_id FROM games WHERE guild = ? AND channel = ? AND active = 1 AND question_id != 0";
				db.get(sql, [trivia.guild.id, trivia.channel.id], (err, row) => {
					
					if(row != null){
						// Flag question
						let sql = "UPDATE questions SET review = 1 WHERE id = (?)";
						db.run(sql, [trivia.guild.id, trivia.channel.id, row.question_id], (err) => {
							// Skip to the next question
							trivia.message.reply("Question flagged for review.");
							trivia.skipQuestion();
						});	
					}
				});	
			}
		});

	}
	
	/**
	 * What happens if no one answers the question in time
	 * @param {*} question_id 
	 */
	timeout(question_id) {

		let trivia = this;

		// Connect to DB
    	let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				reject(err);
			} else {

				// See if we're still waiting for an answer, has the timeout window passed?
				let sql = "SELECT * FROM games WHERE guild = ? AND channel = ?";
		    	db.get(sql, [trivia.guild.id, trivia.channel.id], (err, row) => {
					if (err) {
						reject(err.message);
					} else {
						// Is this question still active?
						if(row != null && row.question_id == question_id){ 
							let current_time = Date.now() / 1000;
							let elapsed_time = current_time - row.asked_at;

							// Is this question timed out?
							if(elapsed_time >= trivia.secondsToAnswerQuestions){
								
								let response = "";
								response += `Times up! The answer was **${trivia.toTitleCase(row.answer)}**.`;
								response += ` New question in ${trivia.secondsBeforeNextQuestion}s`;
								trivia.message.channel.send(response);

								// Set game as no question
								trivia.unloadQuestion().then( unloaded => {
									// Start a new question in x seconds
									let delay = trivia.secondsBeforeNextQuestion * 1000;
									let k = setTimeout(function(){trivia.sendQuestion()}, delay);
								});
							}
						}
					}
				});	
			}
		});

	}

    /**
	 * What happens when someone gets the answer correct
	 * @param {*} answer 
	 * @param {*} asked_at 
	 * @param {*} question_id 
	 * @param {*} answered_at 
	 */
    correctAnswer(answer, asked_at, question_id, answered_at) {

    	let trivia = this;

		// Stop the question timeout
		clearTimeout(this.timeoutTracker);

    	// Connect to DB
    	let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				trivia.error(err);
			} else {

				// Update game
				let sql = "UPDATE games SET question_id = 0, question = '', answer = '', asked_at = 0 WHERE guild = ? AND channel = ?";
				db.run(sql, [trivia.guild.id, trivia.channel.id]);	

				// Get question difficulty
				sql = "SELECT difficulty FROM questions WHERE id = ?";
				db.get(sql, [question_id], (err, row) => {

					// Get difficulty multiplier
					let question_difficulty = row.difficulty;
					let difficulty_multipler = 1;
					switch(question_difficulty.toLowerCase().trim()) {
						default: case 'easy': break;
						case 'medium': 	   difficulty_multipler = 2; break;
						case 'hard': 	   difficulty_multipler = 3; break;
						case 'impossible': difficulty_multipler = 4; break;
					}

					// Calculate score increase
					let max_question_value = difficulty_multipler * trivia.questionValue;
					let seconds_elapsed = (Date.now() / 1000) - asked_at;
					let seconds_remaining = trivia.secondsToAnswerQuestions - seconds_elapsed;
					let pct_of_max_value = seconds_remaining / trivia.secondsToAnswerQuestions;
					let points_earned = Math.round(pct_of_max_value * max_question_value);

					// Prevent out of sync madness
					if(points_earned <= 0){
						return;
					}

					// Increase score - does score record for this user/guild exist yet?
					sql = "SELECT points FROM scores WHERE user = ? AND guild = ?";
					db.get(sql, [trivia.message.author.id, trivia.guild.id], (err, row) => {
						if (err) {
							trivia.error(err);
						} else {
							if(row == null){ // No record exists, create it
								sql = "INSERT INTO scores (guild, user, points, rank) VALUES (?, ?, ?, ?)";
								db.run(sql, [trivia.guild.id, trivia.message.author.id, points_earned, 1]);
							} else { // Record exists, update it
								
								sql = "UPDATE scores SET points = points + ? WHERE guild = ? AND user = ?";
								db.run(sql, [points_earned, trivia.guild.id, trivia.message.author.id]);

								// Does the user's new score qualify for a new rank?
								// Get current rank
								sql = "SELECT * FROM scores WHERE guild = ? AND user = ?";
								db.get(sql, [trivia.guild.id, trivia.message.author.id], (err, row) => {
									let next_rank_id = row.rank + 1;
									let user_points = row.points;
									
									// Get next rank
									sql = "SELECT multiple, name FROM ranks WHERE id = ?";
									db.get(sql, [next_rank_id], (err, row) => {
										let next_rank_points_required = trivia.questionValue * row.multiple;
										let next_rank_name = row.name;

										// Did they qualify?
										if(user_points >= next_rank_points_required){

											// Log new rank
											sql = "UPDATE scores SET rank = ? WHERE guild = ? AND user = ?";
											db.run(sql, [next_rank_id, trivia.guild.id, trivia.message.author.id]);

											// Inform of new rank
											trivia.message.reply(`Congratulations! Your rank has moved up to **${next_rank_name}**!`);

										}
									})
								});
							}
						}
					});	
				
					// Reply with confirmation
					let response = "";
					response = `${trivia.toTitleCase(answer)} is correct! You've earned ${points_earned} points.`;
					response += ` Next question in ${trivia.secondsBeforeNextQuestion} seconds.`;
					trivia.message.reply(response);

					// Send another question if game still active
					let delay = trivia.secondsBeforeNextQuestion * 1000;
					let k = setTimeout(function(){trivia.sendQuestion()}, delay);
				});
			}
		});

    }

    /**
	 * Mask an answer (e.g. "Dog" => "***")
	 * @param {*} answer 
	 */
    maskAnswer(answer, reveal_percent) {

    	return new Promise((resolve, reject) => {

    		let masked   	  = "";
			let char 	 	  = "";
			let need_unmasked = 0;
			let left_unmasked = 0;
    		let ignore   	  = [" ", "-", "&", "/", ",", "'", '+', '.', ":", ";", ")", "(", "$"]; // Don't mask these characters

			// If this is a hint, don't mask some characters
			if(reveal_percent != null && answer.length > 2){		
				need_unmasked = Math.round(answer.length * reveal_percent * .75);

				// Force a reveal of a single character if we would otherwise reveal nonw
				if(need_unmasked == 0){need_unmasked = 1;}

			}

			// Create a mask
    		for (var i = 0; i < answer.length; i++) {
  				char = answer.charAt(i);

  				if(ignore.includes(char)){
  					masked += char;
  				} else {
					// Need to leave some unmasked for a hint?
					if(need_unmasked > left_unmasked){
						masked += char;
						left_unmasked++;
					} else {
						masked += "*";
					}
  				}
			}

			resolve(masked);

    	});

	}
	
	/**
	 * Send a hint to the current game
	 */
	sendHint(question_id) {

		let trivia = this;

		// Connect to DB
    	let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				trivia.error(err);
			} else {

				let sql = "SELECT * FROM games WHERE guild = ? AND channel = ?";
				db.get(sql, [trivia.guild.id, trivia.channel.id], (err, row) => {

					// If question_id was specified, dont't continue unless game is currently on that question
					if(question_id != null && parseInt(row.question_id) != parseInt(question_id)){
						return;
					} else {

						// How much time is left?
						let question_expires  = row.asked_at + trivia.secondsToAnswerQuestions;
						let seconds_remaining = Math.round(question_expires - (Date.now() / 1000));

						// Enough time left to warrant sending a hint?
						if(seconds_remaining >= 2){

							// How much should be revealed int he hint? Less time remaiing = more
							let percent_remaining = seconds_remaining / trivia.secondsToAnswerQuestions;
							let reveal_percent 	  = 1 - percent_remaining;
							
							this.maskAnswer(row.answer, reveal_percent).then( masked => {
								trivia.message.channel.send(`Hint: \`${masked}\` | ${seconds_remaining}s remaining`);
							});
						}

					}	
				});
			}
		});
	}

    /**
	 * Get current active state for guild/channel
	 */
    gameIsActive(){

    	let trivia = this;
    	return new Promise((resolve, reject) => {

		    // Connect to DB
	    	let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
				if (err) {
					reject(err);
				} else {

					// Load the game from the DB
					let sql = "SELECT * FROM games WHERE guild = ? AND channel = ?";
					db.get(sql, [trivia.guild.id, trivia.channel.id], (err, row) => {
						if (err) {
							reject(err.message);
						} else {

							// Does game exist?
							if(row == null){ // Game doesn't exist

								// Create record for Game
								trivia.createGame();

								// Obviously game is not active since it didn't exist
								resolve(false);

							} else { // Game does exist, return active state
								resolve(row.active);
							}
						}
					});	
				}
			}); 
		});			
    }

    /**
	 * Set the activate state of this game
	 * @param {*} state 
	 */
    setActiveState(state) {
    	
    	let trivia = this;
    	let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				trivia.error(err.message);
			} else {
				if(state) { // Start
					db.run("UPDATE games SET active = 1 WHERE guild = ? AND channel = ?", [trivia.guild.id, trivia.channel.id], function(err){
						if(err){trivia.error(err);}
					});
		      	} else { // Stop
		      		db.run("UPDATE games SET active = 0, question_count = 0, question = '', answer = '', question_id = null WHERE guild = ? AND channel = ?", [trivia.guild.id, trivia.channel.id], function(err){
						if(err){trivia.error(err);}
					});
		      	}
			}
		});
    };

    /**
	 * Create a game for my message's guild/channel
	 */
    createGame() {

    	let trivia = this;

    	// Connect to DB
    	let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
		  if (err) {
		    trivia.error(err.message);
		  } else {

		  	// Insert row for my message
	    	let create_bindings = [trivia.guild.id, trivia.channel.id, 0]
	  		db.run("INSERT INTO `games` (guild, channel, active) VALUES (?, ?, ?)", create_bindings, function(err){
	  			if(err){
	  				trivia.error(err);
	  			}
	  		});
	  	  }
	  	});
	}
	
	/**
	 * Output the leaderboard
	 */
	leaderboard() {
		let trivia = this;

		// Connect to DB
		let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				trivia.error(err);
			} else {

				// Get top scores
				let sql = "SELECT * FROM scores WHERE guild = ? ORDER BY points DESC LIMIT 10";
				db.all(sql, [trivia.guild.id], (err, rows) => {
					if (err) {
						reject(err.message);
					} else {
						
						if(rows !== null){
							// Build embed message string
							let desc = ``;
							let cnt = 0;
							let embed = new Discord.RichEmbed();

								// For each score returned
								desc += rows.map(function(row){
									cnt++;
									var user_id = row.user;
									var points = row.points;
									var user_name = "";

									// Get user name
									let member = trivia.guild.members.find(gm => {if(gm.id == user_id){return gm;}});
									if(member != null && member.nickname !== null){
										user_name = member.nickname;
									} else {
										let user = trivia.message.client.users.find(function(user){
											if(user.id == user_id){
												user_name = user.username;
											}
										});
									}
									
									// Add string to output
									return `\n **${cnt}. ${user_name}**: ${points}`;
								});
						
							embed.setDescription(desc);
							trivia.message.channel.send(`Current Trivia leaderboard for ${trivia.guild.name}`, embed);
						} else {
							trivia.message.reply("No leaders found!");
						}
					}
				});	
			}
		}); 
		db.close();
	}

	/**
	 * Convert a string to 'Title Case'.
	 * @param {*} str 
	 */
	toTitleCase(str) {
		return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}

	/**
	 * For a given UserID get their Nickname for this guild. Or, failing that, their username.
	 * @param {*} user_id 
	 */
	getUserNameFromID(user_id) {
		let trivia = this;
		return new Promise((resolve, reject) => {
			let member = trivia.guild.members.get(user_id);
			if(typeof member !== "undefined" && member.nickname !== null){
				resolve(member.nickname);
			} else {
				let user = trivia.message.client.users.find(function(user){
					if(user.id == user_id){
						resolve(user.username);
					}
				});
			}
		});
	}

    /**
	 * Handle errors
	 * @param {*} error 
	 */
    error(error) {
      	console.log("--CRASH--", error);
		this.message.channel.send("😦 Trivia crashed!");
		  
		// Try to stop the game on crash
		let trivia = this;
		let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				trivia.error(err.message);
			} else {
				db.run("UPDATE games SET active = 0, question_count = 0, question = '', answer = '', question_id = null WHERE guild = ? AND channel = ?", [trivia.guild.id, trivia.channel.id], function(err){
					if(err){trivia.error(err);}
				});

			}
		});
    };

    /**
	 * Debug output
	 * @param {*} out 
	 */
    debug(out) {
    	if(this.debug){
    		if(["string", "integer", "boolean"].includes(typeof out)){
    			console.log("DEBUG: "+ out);		
    		} else {
    			console.log("DEBUG: object");
    			console.log(out);
    		}
    		
    	}
	};
	
	/**
	 * Output a list of my commands
	 */
	commands() {
		let desc = "";
		let pre  = conf.prefix;

		desc += `\n${pre}flag [reason] | Skip current question and flag it for review`;
		desc += `\n${pre}help (${pre}commands) | Show this list`;
		desc += `\n${pre}hint | Get a hint for the current question`;
		desc += `\n${pre}leaderboard (${pre}leaders) | Show Trivia Leaderboard`;
		desc += `\n${pre}ranks | Show Ranks list`;
		desc += `\n${pre}skip | Skip current question`;
		desc += `\n${pre}stats [@user] | Get Trivia stats for yourself or another user`;
		desc += `\n${pre}trivia (${pre}t) | Start/Stop trivia`;
		desc += `\n${pre}triviastats | Show Trivia stats`;
		
		let embed = new Discord.RichEmbed();
		embed.setDescription(desc);
		this.message.reply("Command list", embed);
	}

	/**
	 * Show a list of Ranks and their details
	 */
	listRanks() {
		let trivia = this;
		let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				trivia.error(err.message);
			} else {
				db.all("SELECT * FROM ranks ORDER BY id ASC", [], function(err, rows){
					if(err){trivia.error(err);} else {
						
						let desc = "";
						desc += rows.map(function(row){
							var points = row.multiple * trivia.questionValue;
							return `\n${row.id}. ${row.name} (${points} points)`;
						});

						let embed = new Discord.RichEmbed();
						embed.setDescription(desc);
						trivia.message.reply("Rank list", embed);
					}
				});

			}
		});
	}

	/**
	 * Output user stats
	 */
	userStats() {

		let message = this.message;
		let user = null;
		let trivia = this;

		// Get self or another user?
		if (!message.mentions.users.size) {
			user = message.author;
		} else {
			user = message.mentions.users.first();
		}

		let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				trivia.error(err.message);
			} else {
				db.get("SELECT scores.*, ranks.name FROM scores JOIN ranks ON ranks.id = scores.rank WHERE scores.guild = ? AND scores.user = ?", [message.guild.id, user.id], function(err, row){
					if(err){trivia.error(err);} else {
						if(row != null) {
							let rank_name = row.name;
							let points = row.points;
							let user_name = "";
							let user_id = user.id;
							let embed = new Discord.RichEmbed();

							// Get username
							let member = trivia.guild.members.get(user_id);
							if(typeof member !== "undefined" && member.nickname !== null){
								user_name = member.nickname;
							} else {
								let user = trivia.message.client.users.find(function(user){
									if(user.id == user_id){
										user_name = user.username;
									}
								});
							}

							embed.setDescription(`**${user_name}** - ${points} (${rank_name})`);
							message.channel.send("User details", embed);
						} else {
							message.reply("I don't seem to have any information on that user.");
						}
					}
				});

			}
		});
	}

	/**
	 * Get stats for overall Trivia bot
	 */
	triviaStats() {
		
		let trivia = this;

		let db = new sqlite3.Database(this.dbpath, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				trivia.error(err.message);
			} else {

				let embed = new Discord.RichEmbed();
				let desc = ``

				// Question count
				db.get("SELECT count(id) AS count, (SELECT count(id) FROM questions WHERE review = 1) AS flagged FROM questions", [], function(err, row){
					if(err){trivia.error(err);} else {
						if(row != null) {
							
							let questions = row.count;
							let flagged = row.flagged;
							
							// Category count
							db.get("SELECT count(id) AS count FROM question_categories", [], function(err, row){
								if(err){trivia.error(err);} else {
									if(row != null) {
										
										let categories = row.count;
										
										// Users 
										db.get("SELECT count(scores.points) AS count FROM scores", [], function(err, row){
											if(err){trivia.error(err);} else {
												if(row != null) {
													
													let scores = row.count;
													
													desc += `\n Questions: ${questions} (${flagged} flagged)`;
													desc += `\n Categories: ${categories}`;
													desc += `\n Players: ${scores}`;
													embed.setDescription(desc);
													trivia.message.channel.send("Trivia Stats", embed);
						
												} else {
													console.log(error);
													trivia.message.reply("Failed to look up stats.");
												}
											}
										});

									} else {
										console.log(error);
										trivia.message.reply("Failed to look up stats.");
									}
								}
							});

						} else {
							console.log(error);
							trivia.message.reply("Failed to look up stats.");
						}
					}
				});

			}
		});
	}

}

module.exports = Trivia;