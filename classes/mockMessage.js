// Require Discord.js library
const Discord = require('discord.js');

class Message {
	
	constructor(msg){
		msg = msg.join(" ");
		this.content = msg;

		let user = {
			client: {ping: 5.9999999999}, 
		    username: "Tester",
		    id: 142390439356399616,
		    send: function(msg){console.log("DM: " + msg)}
		};
		this.author = user;
		
		// Create mentions
		let mentions = new Discord.Collection();
		mentions.set(user.id, user);
		mentions.users = new Discord.Collection();
		mentions.users.set(user.id, user);
		this.mentions = mentions;

		// Create channel
		this.channel = {
	        id: 222222,
	    	type: "text",
	    	send: function(msg, embed){
				console.log("Output: "+ msg);
				if(typeof embed !== 'undefined'){
					console.log(embed.description);
				}
			},
	        setTopic: function(msg){console.log("Setting topic to: "+msg)}
	    };

		// Create users
		let users = new Discord.Collection();
		users.set(user.id, user);
	
	    // Create client
	    this.client = {
			users: users
		},

	    // Create guild
	    this.guild = {
	        id: 111111,
	    	name: "Test Guild",
			memberCount: 1,
			members: new Discord.Collection(),
	        fetchAuditLogs: function(){return [{}];}
	    };
	}

    // Functions
    reply(msg, embed){
		console.log("Message Reply: " + msg);
		if(typeof embed !== 'undefined'){
			console.log(embed.description);
		}
	}
    delete(){console.log('Message deleted');}
    react(msg){console.log(`Reacted ${msg} to message`);}
}

module.exports = Message;