/**
 * Script to pull data from the OpenTriviaDB API and populate the question databae.
 */


const sqlite3 = require('sqlite3').verbose();
const db_path = "./db/data.sqlite";
const number_of_questions_to_fetch  = 50; // 10 to 50
const seconds_until_repeat_fetch    = 10;
const url = `https://opentdb.com/api.php?amount=${number_of_questions_to_fetch}&type=multiple`;


// Start the script
getQuestions();

// Convert HTML entities to text
function format(str) {
    str = str.replace(/&quot;/gi, "\"");
    str = str.replace(/&eacute;/gi, "e");
    str = str.replace(/&#039;/gi, "'");
    str = str.replace(/&rsquo;/gi, "'");
    str = str.replace(/&amp;/gi, "&");
    return str;
}

function getQuestions() {
    // Create request
    var request = require('request');

    // Configure the request
    var options = {
        url: url,
        method: 'GET',
        headers: {},
    }

    // Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {

            try {
                let obj 	  = JSON.parse(body);
                let questions = obj.results;
                questions.forEach(question => {

                    var question_text   = format(question.question);
                    var cat_text        = format(question.category);
                    var answer_text     = format(question.correct_answer);

                    // Skip these cateogires & long questions
                    if(!question.category.toLowerCase().includes("manga") && answer_text.length < 35){

                        // Connect to DB
                        let db = new sqlite3.Database(db_path, sqlite3.OPEN_READWRITE, (err) => {
                            if (err) {
                                console.error(err);
                            } else {
                                
                                // Question already exists?
                                let sql = "SELECT id FROM questions WHERE question = ?";
                                db.get(sql, [question_text], (err, row) => {
                                    if (err) {console.error(err.message);} else {
                                        
                                        // Question doesn't exist
                                        if(row == null){

                                            // Get category ID
                                            let sql = "SELECT id FROM question_categories WHERE name = ?";
                                            db.get(sql, [cat_text], (err, row) => {
                                                if (err) {console.error(err.message);} else {
                                                    if(row == null){
                                                        console.log(`----- FAILED TO FIND CATEGORY FOR '${cat_text}' -----`);
                                                    } else {
                                                        let cat_id = row.id;

                                                        // Insert question
                                                        let sql = "INSERT INTO questions (type, difficulty, category, question, answer) VALUES (1, ?, ?, ?, ?)";
                                                        db.run(sql, [question.difficulty, cat_id, question_text, answer_text], () => {
                                                            console.log(`Inserted: ${cat_text} - ${question_text}`);
                                                        });
                                                    }
                                                }
                                            });
                                        } else {
                                            console.log("-- Already Exists --");
                                        }
                                    }
                                });	
                            }
                        }); 
                        db.close();
                    }
                    

                });

            }
            catch(error) {
                console.error(error);
            }

            
        } else {
            console.error(error);
        }
    });

    // Continue running every x seconds 
    setTimeout(function(){getQuestions();}, seconds_until_repeat_fetch * 1000);

} 