/**
 * Create the Trivia database
 */

const sqlite3  = require('sqlite3').verbose();
const fs 	   = require("fs");
const readline = require('readline');
const db_path  = "./db/data.sqlite";

// Define a ReadLine instance
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// File exists?
if (fs.existsSync(db_path)) {

	// Ask the user to verify before proceeding
	rl.question('Are you sure you want to recreate the database? THIS WILL ERASE ALL EXISTING SCORE DATA AND QUESTIONS. (yes|no)', (answer) => {
	  	answer = answer.toString().toLowerCase();
	  	if(answer === "yes" || answer === "y"){

		  	// Delete existing file
			fs.unlink(`${db_path}`, function(err){
		  		if (err && err.errno == -2) {
		  		} else {
		  			console.log("Deleting old data file.");
		  		}

				createDB();
			});
		}

	  	rl.close();
	});

	return;

} else {
	return createDB();
}

return;

// Create DB File
function createDB() {
	// Create new data file
	fs.writeFile(`${db_path}`, '', function (err) {
		
		if (err) throw err;
		console.log('New data file created');

		// Create DB structure
		let db = new sqlite3.Database(db_path, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				console.error("Error: " + err.message);
			} else {

				// Scores
				db.run("CREATE TABLE IF NOT EXISTS `scores` (guild INTEGER, user INTEGER, points INTEGER, rank INTEGER)", function(){
					db.run("CREATE INDEX IF NOT EXISTS `guild_index` ON `scores` (`guild`);");
					db.run("CREATE INDEX IF NOT EXISTS `user_index` ON `scores` (`user`);");
				});

				// Ranks
				db.run("CREATE TABLE IF NOT EXISTS `ranks` (id INTEGER PRIMARY KEY, name TEXT, multiple INTEGER)", function(){
					db.run("INSERT INTO ranks (name, multiple) VALUES ('Lucky Guesser', 0)");
					db.run("INSERT INTO ranks (name, multiple) VALUES ('Novice', 5)");
					db.run("INSERT INTO ranks (name, multiple) VALUES ('Amateur', 15)");
					db.run("INSERT INTO ranks (name, multiple) VALUES ('Average', 25)");
					db.run("INSERT INTO ranks (name, multiple) VALUES ('Smart', 45)");
					db.run("INSERT INTO ranks (name, multiple) VALUES ('Talented', 65)");
					db.run("INSERT INTO ranks (name, multiple) VALUES ('Expert', 95)");
					db.run("INSERT INTO ranks (name, multiple) VALUES ('Master', 125)");
					db.run("INSERT INTO ranks (name, multiple) VALUES ('Brilliant', 165)");
					db.run("INSERT INTO ranks (name, multiple) VALUES ('Prodigy', 205)");
					db.run("INSERT INTO ranks (name, multiple) VALUES ('Genius', 255)");
					db.run("INSERT INTO ranks (name, multiple) VALUES ('Trivia God', 500)");
					db.run("INSERT INTO ranks (name, multiple) VALUES ('The All Knowing', 5000)");
				});
				
				// Games
				db.run("CREATE TABLE IF NOT EXISTS `games` (guild INTEGER, channel INTEGER, active INTEGER, question_id INTEGER, question TEXT, answer TEXT, asked_at INTEGER, question_count INTEGER)", function(){
					db.run("CREATE INDEX IF NOT EXISTS `guild_index` ON `games` (`guild`);");	
				});

				// Question Types
				db.run("CREATE TABLE IF NOT EXISTS `question_types` (id INTEGER PRIMARY KEY, name TEXT)", function(){
					db.run("INSERT INTO question_types (name) VALUES ('Standard')");
					db.run("INSERT INTO question_types (name) VALUES ('Word Scramble')");
					db.run("INSERT INTO question_types (name) VALUES ('Multiple Answer')");
				});				

				// Categories
				db.run("CREATE TABLE IF NOT EXISTS `question_categories` (id INTEGER PRIMARY KEY, name TEXT)", function(){
					
					// Define categories
					let cats = [
						'General Knowledge',
						'Entertainment: Books',
						'Entertainment: Film',
						'Entertainment: Music',
						'Entertainment: Musicals & Theatres',
						'Entertainment: Television',
						'Entertainment: Video Games',
						'Entertainment: Board Games',
						'Entertainment: Cartoon & Animations',
						'Entertainment: Comics',
						'Science & Nature',
						'Science: Computers',
						'Science: Mathematics',
						'Science: Gadgets',
						'Mythology',
						'Sports',
						'Geography',
						'History',
						'Politics',
						'Art',
						'Celebrities',
						'Animals',
						'Vehicles',
						'Medicine',
						'Food',
						'Technology',
						'Philosophy'
					];

					cats.forEach(cat => {
						db.run("INSERT INTO question_categories (name) VALUES (?)", [cat]);
					});

				});				

				// Questions
				db.run("CREATE TABLE IF NOT EXISTS `questions` (id INTEGER PRIMARY KEY, difficulty TEXT, category INTEGER, type INTEGER, question TEXT, answer TEXT, review INTEGER DEFAULT 0, review_reason TEXT DEFAULT '')", function(){
					// Add a few sample questions
					db.run("INSERT INTO questions (difficulty, category, type, question, answer) VALUES ('easy',   4,  1, 'Hair metal band: Guns N ?????', 'roses')");
					db.run("INSERT INTO questions (difficulty, category, type, question, answer) VALUES ('easy',   11, 1, 'Who was the first person to walk on the moon?', 'neil armstrong')");
					db.run("INSERT INTO questions (difficulty, category, type, question, answer) VALUES ('easy',   12, 2, 'Unscramble this word: e a t o s f w r', 'software')");
					db.run("INSERT INTO questions (difficulty, category, type, question, answer) VALUES ('medium', 16, 1, 'Who was the first MLB pitcher to throw 100mph?', 'nolan ryan')");
					db.run("INSERT INTO questions (difficulty, category, type, question, answer) VALUES ('easy',   1,  2, 'Unscramble this word: k e b y a r', 'bakery')");
					db.run("INSERT INTO questions (difficulty, category, type, question, answer) VALUES ('easy',   22, 1, 'A popular police K-9 breed from Germany', 'german shepherd')");
					db.run("INSERT INTO questions (difficulty, category, type, question, answer) VALUES ('easy',   3,  1, 'Hollywood actor, famous for his roles as Rick Deckard and Han Solo', 'harrison ford')");
					db.run("INSERT INTO questions (difficulty, category, type, question, answer) VALUES ('easy',   4,  1, 'Name that band: Master of Puppets', 'metallica')");
					db.run("INSERT INTO questions (difficulty, category, type, question, answer) VALUES ('medium', 11, 1, 'In 1993 a liver of this animal was transplanted into a human at the University of Pittsburgh', 'baboon')");
				})

				db.close();
				console.log('Data structure created');
			}
		});
	}); 

	return true;
}