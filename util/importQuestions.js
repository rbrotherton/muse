/**
 * Import a CSV file of questions into the Trivia database
 */

 // Config
const sqlite3   = require('sqlite3').verbose();
const fs 	    = require("fs");
const path      = require('path');
const async     = require('async');
const readline  = require('readline');
const dbpath    = "./db/data.sqlite";

// Import file specifics
const col_delimeter  = "	"; // Tab
const line_delimeter = "\n";
const file_extension = ".tsv"; 

// Variables for script
let categories        = {};
let difficulties      = ["easy", "medium", "hard", "impossible"];
let types             = {};

// Start processing - DB exists?
if (fs.existsSync(dbpath)) {

    // Connect to DB
    let db = new sqlite3.Database(dbpath, sqlite3.OPEN_READWRITE, (err) => {
        if (err) { log(err); return; } else {

            // Load categories
            let sql = `SELECT * FROM question_categories ORDER BY name ASC`;
            db.all(sql, [], (err, rows) => {
                
                categories = rows;

                // Load types
                let sql = `SELECT * FROM question_types ORDER BY name ASC`;
                db.all(sql, [], (err, rows) => {

                    types = rows;

                    // Get data file path from arguments
                    let data_file_path = process.argv[2];

                    // Is data file path valid?
                    if (fs.existsSync(data_file_path) && path.extname(data_file_path) == file_extension){

                        // For each line in file - synchronus to prevent DB locks
                        var lines = fs.readFileSync(data_file_path).toString().split(line_delimeter);
                        async.eachSeries(lines, (line, callback) => {

                            // Turn CSV line into an array
                            let array = line.split(col_delimeter);

                            // Pull data out of array
                            let type          = array[0];
                            let difficulty    = array[1];
                            let category      = array[2];
                            let question_text = array[3];
                            let answer        = array[4];

                            // Import question
                            addQuestionToDatabase(category, difficulty, type, question_text, answer)
                                .then(setTimeout(callback, 100)) // Have to delay to prevent DB locking
                                .catch(err => {
                                    if(err != null){
                                        log(`${err}`);
                                    } else {
                                        log(`addQuestionToDatabase Error: unknown`);
                                    }
                                });

                        });

                    } else {
                        log(`ERROR: Invalid data file path or extension (${file_extension}) specified`);
                        return;
                    }

                });
            });
        }
    });
}

/**
 * Add a question to the database
 * @param {*} category_id 
 * @param {*} difficulty 
 * @param {*} question 
 * @param {*} answer 
 */
function addQuestionToDatabase(input_category, input_difficulty, input_type, input_question, input_answer) {

    return new Promise((resolve, reject) => {
        questionExists(input_question)
            .then( exists => {
                if(exists){
                    reject(`Question already exists: "${input_question}"`);
                } else {
                    // Connect to DB
                    let db = new sqlite3.Database(dbpath, sqlite3.OPEN_READWRITE, (err) => {
                        if (err) {
                            log(err);
                            reject("Failed to connect to database");
                        } else {

                            let category_id       = 0;
                            let type_id           = 0;

                            // Is difficulty valid?
                            input_difficulty  = input_difficulty.toLowerCase().trim();
                            difficultyIsValid = true;
                            if(!difficulties.includes(input_difficulty)){
                                log(`Invalid difficulty ("${input_difficulty}"). Skipping question: "${input_question}"`);
                                reject("Invalid difficulty setting");
                            }
                            
                            // Is category valid
                            let categoryIsValid = false;
                            input_category = input_category.toLowerCase().trim();
                            categories.find(category => {
                                if(category.name.toLowerCase().trim() == input_category){
                                    categoryIsValid = true;
                                    category_id = category.id;
                                }
                            });

                            // Is type valid
                            let typeIsValid = false;
                            input_type = input_type.toLowerCase().trim();
                            types.find(type => {
                                if(type.name.toLowerCase().trim() == input_type){
                                    typeIsValid = true;
                                    type_id = type.id;
                                }
                            });

                            // Insert question is we have valid input
                            if(difficultyIsValid && categoryIsValid && typeIsValid && category_id != 0 && type_id != 0){

                                input_question   = format(input_question);
                                input_answer     = format(input_answer);
                                input_difficulty = input_difficulty.toLowerCase();
                                let sql = `INSERT INTO questions (difficulty, category, type, question, answer) VALUES (?, ?, ?, ?, ?)`;
                                db.run(sql, [input_difficulty, category_id, type_id, input_question, input_answer], (err) => {
                                    if(err != null){
                                        db.close();
                                        reject(`INSERT ERROR (${err.code}): ${input_question}`);
                                    } else {
                                        db.close();
                                        log(`Imported question: ${input_question}`);
                                        resolve("Question inserted");
                                    }
                                });
                            } else {
                                log(`Invalid difficulty (${input_difficulty}) or category (${input_category} | ${category_id}) or type (${input_type} | ${type_id}) - Skipping question "${input_question}"`);
                                reject("Question failed to validate");
                            }
                            
                            return;
                        }
                    });
                }   
            })
            .catch(err => reject(err));
    });
}

/**
 * Does this question already exist?
 * @param {*} question 
 */
function questionExists(question) {

    return new Promise((resolve, reject) => {

        // Connect to DB
        let db = new sqlite3.Database(dbpath, sqlite3.OPEN_READWRITE, (err) => {
            if (err) {
                reject(err);
            } else {

                // See if question is already in table
                let sql = `SELECT * FROM questions WHERE question = ?`;
                db.get(sql, [format(question)], (err, row) => {
                    if (err) {
                        reject(err.message);
                    } else {
                        if(row == null){
                            resolve(false);
                        } else {
                            resolve(true);
                        }
                    }
                });	
            }
        }); 
    });
}

/**
 * Logging function
 * @param {*} mixed 
 */
function log(mixed) {

    let date = new Date();
    let stamp = date.toLocaleString();

    if(typeof mixed == "string"){
        console.log(`[${stamp}] ${mixed}`);
    } else {
        console.log(mixed);
    }
    
}

// Convert HTML entities to text
function format(str) {
    
    if(str == ""){
        return str;
    }

    str = str.replace(/&quot;/gi, "\"");
    str = str.replace(/&eacute;/gi, "e");
    str = str.replace(/&#039;/gi, "'");
    str = str.replace(/&rsquo;/gi, "'");
    str = str.replace(/&amp;/gi, "&");
    return str.trim();
}