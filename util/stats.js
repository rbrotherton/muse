/**
 * A quick analytical glimpse into the db
 */

const sqlite3  = require('sqlite3').verbose();
const db_path  = "./db/data.sqlite";
const fs       = require('fs');

// File exists?
if (fs.existsSync(db_path)) {

    // Conect to DB
    let db = new sqlite3.Database(db_path, sqlite3.OPEN_READWRITE, (err) => {
        if (err) {
            console.error("Error: " + err.message);
        } else {

            // Categories
            db.get("SELECT count(id) AS count FROM question_categories", [], (err, row) => {console.log(`Categories: ${row.count}`);});	
            
            // Questions
            db.get("SELECT count(id) AS count, (SELECT count(id) FROM questions WHERE review= 1) AS flagged FROM questions", [], (err, row) => {console.log(`Questions: ${row.count} (${row.flagged} flagged)`);});	
            
            // Total Games
            db.get("SELECT count(guild) AS count FROM games", [], (err, row) => {console.log(`Total Games: ${row.count}`);});	

            // Active Games
            db.get("SELECT count(guild) AS count FROM games WHERE active = 1", [], (err, row) => {console.log(`Active Games: ${row.count}`);});	

            // Users
            db.get("SELECT count(score) AS count FROM scores", [], (err, row) => {
                if(row != null){
                    console.log(`Users with points: ${row.count}`);
                } else {
                    console.log(`Users with points: 0`);
                }
            });	
        }
    });

} else {
	console.error("Database doesn't exist!");
}

return;