/**
 * Debug script to stop all in-progress games 
 */

const sqlite3  = require('sqlite3').verbose();
const db_path  = "./db/data.sqlite";
const fs       = require('fs');

// File exists?
if (fs.existsSync(db_path)) {

    // Conect to DB
    let db = new sqlite3.Database(db_path, sqlite3.OPEN_READWRITE, (err) => {
        if (err) {
            console.error("Error: " + err.message);
        } else {
            db.run("UPDATE games SET active = 0");	
            console.log("All games stopped");
        }
    });

} else {
	console.error("Database doesn't exist!");
}

return;