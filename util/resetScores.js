/**
 * Utility script to reset all scores
 */

const sqlite3  = require('sqlite3').verbose();
const fs 	   = require("fs");
const readline = require('readline');
const db_path  = "./db/data.sqlite";

// Define a ReadLine instance
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// File exists?
if (fs.existsSync(db_path)) {

	// Ask the user to verify before proceeding
	rl.question('Are you sure you want to reset all of the scores? (yes|no)', (answer) => {
	  	answer = answer.toString().toLowerCase();
	  	if(answer === "yes" || answer === "y"){

            // Conect to DB
		    let db = new sqlite3.Database(db_path, sqlite3.OPEN_READWRITE, (err) => {
                if (err) {
                    console.error("Error: " + err.message);
                } else {

                    // Reset scores
                    db.run("UPDATE scores SET points = 0, rank = 0", []);
                    console.log("Scores reset.");
                }
            });
		}

	  	rl.close();
	});

	return;

} else {
	console.error("Database doesn't exist!");
}

return;