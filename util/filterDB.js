/**
 * Utility script to filter funky characters from the database
 */

const sqlite3  = require('sqlite3').verbose();
const fs 	   = require("fs");
const readline = require('readline');
const db_path  = "./db/data.sqlite";

const replacements = [
    {"find": "&quot;", "replace": "\""},
    {"find": "&eacute;", "replace": "e"},
    {"find": "&#039;", "replace": "'"},
    {"find": "&rsquo;", "replace": "'"},
    {"find": "&amp;", "replace": "&"},
    {"find": "&ouml;", "replace": "o"},
    {"find": "`", "replace": ""},
    {"find": "&aacute;", "replace": "a"},
    {"find": "&iacute;", "replace": "i"},
];

// File exists?
if (fs.existsSync(db_path)) {

    // Conect to DB
    let db = new sqlite3.Database(db_path, sqlite3.OPEN_READWRITE, (err) => {
        if (err) {
            console.error("Error: " + err.message);
        } else {

            // Filter text fields
            replacements.map(rep => {
                db.run("UPDATE questions SET question = replace(question, ?, ?) WHERE question LIKE '%"+ rep.find +"%'", [rep.find, rep.replace]);    
                db.run("UPDATE questions SET answer = replace(answer, ?, ?) WHERE answer LIKE '%"+ rep.find +"%'", [rep.find, rep.replace]);
            });
            
            console.log("Data filtered");
        }
    });

} else {
	console.error("Database doesn't exist!");
}

return;