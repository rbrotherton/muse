# Muse
A simple Trivia bot for Discord written in Node.js.

### Prerequisites

* Node v8 or greater

### Install

1. `git clone https://bitbucket.org/rbrotherton/muse.git`
2. `cd muse`
3. `mkdir db`
4. `npm install`
5. `node util/createDB.js`
6. Create a `config.json` file with the following contents:
```
{
	"prefix": "!",
	"token": "YOUR BOT TOKEN",
	"debugmode": false,
    "secondsToAnswerQuestions": 45, 
    "secondsBeforeNextQuestion": 8, 
    "questionValue": 100 		  
}
```

### Launch App
`node app.js`

### Populate Database
The installation process will create a few sample questions in the database. You're free to fill it with whatever you like. There is an included script that will pull questions from https://opentdb.com/]. To use this, just run `node util/populateDB.js`.

Due to the nature of their API we can only request a random sampling of questions which means we will get duplicates. The script will filter these out. Just let this script run until the output indicates that you're getting all duplicates. At this point you should have 2500+ unique questions.  

### Commands
* `flag [reason]` | Skip current question and flag it for review 
* `help (alias commands)` | Show this list 
* `hint` | Get a hint for the current question 
* `leaderboard (alias leaders)` | Show Trivia Leaderboard 
* `ranks` | Show Ranks list 
* `skip` | Skip current question 
* `stats [@user]` | Get Trivia stats for yourself or another user 
* `trivia (alias t)` | Start/Stop trivia 
* `triviastats` | Show Trivia stats

### Testing

* `node test .t` Start/Stop Trivia
* `node test answer` Test answer while a game is running (from another terminal)
* `node test <command>` Test a command

### Features
* Configurable question timeouts and time periods between questions
* Point multipliers for question difficulty
* Automatic and manual hints
* Skip questions
* Flag questions for review. They will be skipped and not shown again until reviewed.
* Gain ranks as you gain points
* Show a leaderboard of the top 10 players 
* Show points and rank for a single user, or yourself

