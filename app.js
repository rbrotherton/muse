// Include filesystem
const fs = require('fs');

// Require Discord.js library
const Discord = require('discord.js');

// Load config
const { prefix, token } = require('./config.json');

// Create a new Discord client
const client = new Discord.Client();

// On client ready, report status
client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
  client.user.setPresence({
	game: {
	  name: `${prefix}trivia | ${prefix}help`,
	  type: "LISTENING",
	},
  }).then(console.log("Status set")).catch(err => console.log(err));
});

// Handle inbound messages
client.on('message', message => {

	// If message was sent by a bot, ignore message
	if (message.author.bot) return;

	// Pull args and command out of message
	const args = message.content.slice(prefix.length).split(/ +/);
	const commandName = args.shift().toLowerCase();
		
	// Can't be in a DM
	if (message.channel.type !== 'text') {
	    return message.reply('I can\'t operate inside DMs!');
	}

	// Init class and process 
	const triviaClass = require('./classes/trivia.js');
	const trivia = new triviaClass(message);
	trivia.handleMessage();

});

// Login to Discord
client.login(token);