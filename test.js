// Include filesystem
const fs = require('fs');

// Require Discord.js library
const Discord = require('discord.js');

// Load config
const { prefix, token } = require('./config.json');

// Create a new Discord client
const client = new Discord.Client();

// Get args
let args = [];
process.argv.forEach(function (val, index, array) {
  if(index > 1){
  	args[index-2] = val;	
  }
});

// Get Mock message
const mockMessage = require("./classes/mockMessage.js");
const message = new mockMessage(args);

// Init class and process 
const triviaClass = require('./classes/trivia.js');
const trivia = new triviaClass(message);
trivia.handleMessage();